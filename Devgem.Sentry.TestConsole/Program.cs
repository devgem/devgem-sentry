﻿using System;
using DevGem.Sentry;

namespace Devgem.Sentry.TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var attribute = new SentryExceptionFilterAttribute();
            Console.WriteLine(attribute.GetType().FullName);
        }
    }
}