﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SharpRaven.Core;
using SharpRaven.Core.Configuration;

namespace DevGem.Sentry
{
    public static class SentryIoCExtensions
    {
        public static void AddSentry(this IServiceCollection services, string sentryDns, string environment = null,
            string release = null)
        {
            services.Configure<RavenOptions>(options => options.DSN = sentryDns);

            //Add HTTPContextAccessor as Singleton
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Configure RavenClient
            services.AddScoped<IRavenClient, RavenClient>(serviceProvider =>
            {
                var ravenClient = new RavenClient(serviceProvider.GetRequiredService<IOptions<RavenOptions>>(),
                    serviceProvider.GetRequiredService<IHttpContextAccessor>())
                {
                    Environment = environment,
                    Release = release
                };
                return ravenClient;
            });
        }
    }
}