using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpRaven.Core;

namespace DevGem.Sentry.Test
{
    [TestClass]
    public class SentryTests
    {
        private const string RavenDns =
            "https://123452d79ffb4157acd0c0c380330140:12345ac4f47544e68da4cb9b8d9d6fe4@sentry.io/123454";

        [TestMethod]
        public void AddSentry_should_work()
        {
            new ServiceCollection().AddSentry(
                RavenDns,
                "UnitTester", "0.1");
        }

        [TestMethod]
        public void SentryExceptionFilterAttribute_should_create()
        {
            var attribute = new SentryExceptionFilterAttribute();
            Assert.IsNotNull(attribute);
        }

        [TestMethod]
        public void SentryExceptionFilterAttributeImplementation_should_work()
        {
            var assembly = typeof(SentryExceptionFilterAttribute).Assembly;
            var type = assembly.GetType(
                "DevGem.Sentry.SentryExceptionFilterAttribute+SentryExceptionFilterAttributeImplementation");
            var constructors = type.GetConstructors();
            var implementation = constructors[0].Invoke(new object[]
            {
                new RavenClient(RavenDns),
                new LoggerFactory()
            });
            var method = type.GetMethod("OnException");
            method.Invoke(implementation, new object[]
            {
                new ExceptionContext(
                    new ActionContext(new DefaultHttpContext(), new RouteData(), new ActionDescriptor()),
                    new List<IFilterMetadata>())
            });
        }
    }
}