# DevGem.Sentry

[![Build status](https://gitlab.com/devgem/devgem-sentry/badges/master/build.svg)](https://gitlab.com/devgem/devgem-sentry/pipelines)

[![Quality Gate](https://inspect.devgem.be/api/badges/gate?key=devgem-sentry)](https://inspect.devgem.be/dashboard/index/devgem-sentry) [![Lines of Code](https://inspect.devgem.be/api/badges/measure?key=devgem-sentry&metric=ncloc)](https://inspect.devgem.be/component_measures?id=devgem-sentry&metric=ncloc) [![Code Coverage](https://inspect.devgem.be/api/badges/measure?key=devgem-sentry&metric=coverage)](https://inspect.devgem.be/component_measures?id=devgem-sentry&metric=Coverage) [![Technical Debt Ratio](https://inspect.devgem.be/api/badges/measure?key=devgem-sentry&metric=sqale_debt_ratio)](https://inspect.devgem.be/component_measures?id=devgem-sentry) [![Security](https://img.shields.io/badge/security-dependency--check-lightgrey.svg)](https://inspect.devgem.be/project/extension/dependencycheck/report?id=devgem-sentry)

[![NuGet Version](https://img.shields.io/nuget/v/DevGem.Sentry.svg)](https://www.nuget.org/packages/Devgem.Sentry/) [![NuGet Downloads](https://img.shields.io/nuget/dt/DevGem.Sentry.svg)](https://www.nuget.org/packages/Devgem.Sentry/)


## Overview

.NET Core utility library for [`RavenSharp.Core`](https://www.nuget.org/packages/RavenSharp.Core/) containing ASP.NET Core filters and IoC to send exceptions to [`Sentry`](https://sentry.io).

## Usage

### Configuration

In `Startup.cs`:

```csharp
using DevGem.Sentry;
...
       public void ConfigureServices(IServiceCollection services)
       {
            // define the Sentry settings as constants or get them from your configuration
            // the environment and release arguments are optional
            services.AddSentry(YOUR_SENTRYDNS, YOUR_ENVIRONMENT, YOUR_RELEASE);
            ...
```
### Code

In some `controller`:

```csharp
using DevGem.Sentry;
...
    // if you put the SentryExceptionFilter attribute on a controller
    // then all exceptions thrown by the controller will be reported to Sentry
    [SentryExceptionFilter]
    public class SomeController : Controller
    {
      ...
```

## Get it!

You can clone and build DevGem.Sentry yourself from [https://gitlab.com/devgem/devgem-sentry/](https://gitlab.com/devgem/devgem-sentry/), but for those of us who are happy with prebuilt binaries, there's a NuGet package: [https://www.nuget.org/packages/Devgem.Sentry/](https://www.nuget.org/packages/Devgem.Sentry/).

| Version | Remarks |
|-----|-----|
| [`2018.327.2132`](https://www.nuget.org/packages/Devgem.Sentry/2018.327.2132) | Updated Microsoft.AspNetCore from 2.0.5 to 2.0.6 |
| [`2018.305.1229`](https://www.nuget.org/packages/Devgem.Sentry/2018.305.1229) | Initial version |
| [`2018.122.3`](https://www.nuget.org/packages/Devgem.Sentry/2018.122.3) | Test release |
